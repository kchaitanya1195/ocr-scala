package controllers

import com.recognition.software.jdeskew.{ImageDeskew, ImageUtil}
import net.sourceforge.tess4j.util.ImageHelper
import org.bytedeco.javacpp.indexer.UByteRawIndexer
import org.bytedeco.javacpp.opencv_core.{CV_8UC, Mat}
import org.bytedeco.javacpp.{opencv_photo => Photo}
import org.bytedeco.javacv.Java2DFrameUtils
import play.api.libs.Files.TemporaryFile
import play.api.mvc._

import java.awt.image.BufferedImage
import java.io.File
import java.nio.file.Files
import javax.imageio.ImageIO
import javax.inject._

@Singleton
class OCRController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with OCR {

  def image: Action[MultipartFormData[TemporaryFile]] =
    Action(parse.multipartFormData) { implicit request =>
      request.body.file("image")
        .map(_.ref.path)
        .map(Files.newInputStream(_))
        .map(ImageIO.read)
        .map(ImageHelper.convertImageToBinary)
        .map(imageDeSkew(_))
        .map(biToMat)
        .map(enhance)
        .map(matToBi)
        .map(imageWriter)
      match {
        //case Some(ocr: String) => Ok(ocr)
        case Some(_) => Ok("File processed")
        case None => BadRequest("No image found")
      }
    }

  private def imageDeSkew(bi: BufferedImage, skewThreshold: Double = 0.05) = {
    val imageSkewAngle = new ImageDeskew(bi).getSkewAngle

    if (imageSkewAngle > skewThreshold || imageSkewAngle < -skewThreshold) {
      ImageUtil.rotate(bi, -imageSkewAngle, bi.getWidth() / 2, bi.getHeight() / 2)
    } else {
      bi
    }
  }

  private def imageWriter(bi: BufferedImage) = {
    val out = new File("output_image.jpeg")
    ImageIO.write(bi, "jpeg", out)
    bi
  }

  private def imageOcr(bi: BufferedImage) = {
    tesseract.doOCR(bi)
  }

  private def biToMat(bi: BufferedImage) = {
    val mat = new Mat(bi.getHeight, bi.getWidth, CV_8UC(3))
    val indexer:UByteRawIndexer = mat.createIndexer()
    for (y <- 0 until bi.getHeight()) {
      for (x <- 0 until bi.getWidth()) {
        val rgb = bi.getRGB(x, y)
        indexer.put(y, x, 0, (rgb >> 0) & 0xFF)
        indexer.put(y, x, 1, (rgb >> 8) & 0xFF)
        indexer.put(y, x, 2, (rgb >> 16) & 0xFF)
      }
    }
    indexer.release()
    mat
  }

  private def matToBi(mat: Mat) = Java2DFrameUtils.toBufferedImage(mat)

  private def deNoise(mat: Mat) = {
    val dst = mat.clone()
    Photo.fastNlMeansDenoising(mat, dst, 40, 10, 40)
    dst
  }

  private def enhance(mat: Mat) = {
    val dst = mat.clone()
    Photo.detailEnhance(mat, dst)
    dst
  }
}
