package controllers

import net.sourceforge.tess4j.Tesseract

trait OCR {
  val tesseract: Tesseract = new Tesseract
  tesseract.setDatapath("/usr/local/Cellar/tesseract/4.1.1/share/tessdata")
}
