name := """ocr-scala"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.4"

val akkaVersion = "2.6.12"
libraryDependencies ++= Seq(
  guice,
  "net.sourceforge.tess4j" % "tess4j" % "4.5.4",
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  "com.typesafe.akka" %% "akka-serialization-jackson" % akkaVersion
)

javaCppPlatform := Seq("macosx-x86_64")
